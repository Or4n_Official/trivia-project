#pragma once
#include "Database.h"
#include <vector>

class LoggedUser
{
public:
	LoggedUser(string username);
	string GetUsername() const;
	bool operator==(const LoggedUser& rhs);
private:
	string _username;
};

class LoginManager
{
public:
	LoginManager(Database* db);

	/*
	adds user to database and returns return code
	input: username password and email
	output: return code
	*/
	int signup(string username, string password, string email);

	/*
	logs user in and returns the result
	input: username, password
	output: return code
	*/
	int login(string username, string password);

	/*
	logs out (removes user from logged users list)
	input: username
	output: none
	*/
	void logout(string username);
private:
	Database* _db;
	std::vector<LoggedUser> _loggedUsers;
};