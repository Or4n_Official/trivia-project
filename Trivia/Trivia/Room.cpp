#include "Room.h"

Room::Room()
{
}

Room::Room(RoomData roomdata, LoggedUser creatingUser)
	: _metadata(roomdata)
{
	_users.push_back(creatingUser);
}

void Room::addUser(LoggedUser user)
{
	vector<LoggedUser>::iterator it = std::find(this->_users.begin(), this->_users.end(), user);
	if (it == _users.end())
	{
		return;
	}
	_users.push_back(user);
}

void Room::removeUser(LoggedUser user)
{
	vector<LoggedUser>::iterator it = std::find(this->_users.begin(), this->_users.end(), user);
	if (it != _users.end())
	{
		_users.erase(it);
	}
}

vector<string> Room::getAllUsers()
{
	vector<string> usernames;
	for (auto it = _users.begin(); it != _users.end(); it++)
	{
		usernames.push_back(it->GetUsername());
	}
	return usernames;
}

RoomData Room::getRoomData()
{
	return _metadata;
}
