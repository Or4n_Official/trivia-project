#include <iostream>
#include <string>
#include <nlohmann/json.hpp>
using json = nlohmann::json;
using std::string;

typedef struct loginRequest
{
	string username;
	string password;
}loginRequest;
typedef struct signupRequest
{
	string username;
	string password;
	string email;
}signupRequest;
typedef struct getPlayersInRoomRequest
{
	unsigned int roomId;
}getPlayersInRoomRequest;
typedef struct joinRoomRequest
{
	unsigned int roomId;
}joinRoomRequest;
typedef struct createRoomRequest
{
	string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
}createRoomRequest;
typedef struct submitAnswerRequest
{
	unsigned int answerId;
}submitAnswerRequest;