#pragma once
#include <iostream>
#include <string>
#include <nlohmann/json.hpp>
#include "structsDes.cpp"
using json = nlohmann::json;
using std::string;

#define SIZE 1024

class JsonRequestPacketDeserializer
{
public:
	static string bufferToString(unsigned char* buffer);
	static json stringToJson(string buffer);
	static json bufferToJson(unsigned char* buffer);
	static loginRequest deserializeLoginRequest(unsigned char* buffer);
	static signupRequest deserializeSignupRequest(unsigned char* buffer);
	static getPlayersInRoomRequest deserializeGetPlayerRequest(unsigned char* buffer);
	static joinRoomRequest deserializeJoinRoomRequest(unsigned char* buffer);
	static createRoomRequest deserializeCreateRoomRequest(unsigned char* buffer);
	static submitAnswerRequest deserializeSubmitAnswerRequest(unsigned char* buffer);
};

