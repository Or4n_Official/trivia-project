#pragma once
#include <ctime>
#include "PacketSerializer.h"

class IRequestHandler;
struct RequestInfo
{
	int requestId;
	time_t recievalTime;
	BYTE_STREAM buffer;
};
struct RequestResult
{
	BYTE_STREAM buffer;
	IRequestHandler* newHandler;
};
class IRequestHandler
{
public:
	virtual bool isRequestRelevant(RequestInfo req) = 0;
	virtual RequestResult HandleRequest(RequestInfo req) = 0;
};