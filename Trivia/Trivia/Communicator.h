#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include <vector>
#include <string>
#include <map>
#include <thread>
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "LoginRequestHandler.h"

#define PORT 7777

using std::map;

class Communicator
{
public:

	Communicator();
	Communicator(RequestHandlerFactory* f);
	~Communicator();

	/*
	this method starts the communicator up starts handling incoming clients
	input: nothing
	output: nothing
	*/
	void startHandleRequests();
private:
	RequestHandlerFactory* _factory;
	SOCKET _serverSocket;
	map<SOCKET, IRequestHandler*> _clients;

	/*
	this method binds the socket and listens for new clients, when a client is accepted, a thread opens that handles that client
	input: none
	output: none
	*/
	void bindAndListen();

	/*
	the main conversation with each client
	input: client socket
	output: None
	*/
	void handleNewClient(SOCKET clientSock);
};
