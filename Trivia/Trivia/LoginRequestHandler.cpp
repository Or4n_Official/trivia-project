#include "LoginRequestHandler.h"

RequestResult LoginRequestHandler::login(RequestInfo info)
{
    RequestResult r;
    r.newHandler = this;
    unsigned char newBuffer[SIZE] = { 0 };
    std::copy(info.buffer.begin(), info.buffer.end(), newBuffer);
    loginRequest login = JsonRequestPacketDeserializer::deserializeLoginRequest(newBuffer);
    int ans = _loginManager->login(login.username, login.password);
    if (ans == LOGIN_FAILED)
    {
        responses::ErrorResponse er = { "Login Failed" };
        r.buffer = JsonResponsePacketSerializer::serializeResponse(er);
    }
    else if (ans == USER_ALREADY_LOGGED_IN)
    {
        responses::ErrorResponse er = { "Already logged in" };
        r.buffer = JsonResponsePacketSerializer::serializeResponse(er);
    }
    else
    {
        responses::LoginResponse er = { LOGIN_SUCCESSFUL };
        r.buffer = JsonResponsePacketSerializer::serializeResponse(er);
    }
    return r;
}

RequestResult LoginRequestHandler::signup(RequestInfo info)
{
    RequestResult r;
    r.newHandler = this;
    unsigned char newBuffer[SIZE] = { 0 };
    std::copy(info.buffer.begin(), info.buffer.end(), newBuffer);
    signupRequest signup = JsonRequestPacketDeserializer::deserializeSignupRequest(newBuffer);
    int ans = _loginManager->signup(signup.username, signup.password, signup.email);

    if (ans == UNDEFINED_ERROR)
    {
        responses::ErrorResponse er = { "Undefined Error" };
        r.buffer = JsonResponsePacketSerializer::serializeResponse(er);
    }
    else if (ans == USER_EXISTS)
    {
        responses::ErrorResponse er = { "User Exists" };
        r.buffer = JsonResponsePacketSerializer::serializeResponse(er);
    }
    else if (ans == USER_ADDED_SUCCESSFULLY)
    {
        responses::SignupResponse a = { USER_ADDED_SUCCESSFULLY };
        r.buffer = JsonResponsePacketSerializer::serializeResponse(a);
    }
    return r;

}

LoginRequestHandler::LoginRequestHandler(LoginManager* LoginManager, RequestHandlerFactory* handlerFactory)
{
	this->_factory = handlerFactory;
	this->_loginManager = LoginManager;
}

bool LoginRequestHandler::isRequestRelevant(RequestInfo info)
{
	return false;
}

RequestResult LoginRequestHandler::HandleRequest(RequestInfo info)
{
    if (info.requestId == SIGNUP_ID)
    {
        return this->signup(info);
    }
    else if (info.requestId == LOGIN_ID)
    {
        return this->login(info);
    }
	return RequestResult();
}
