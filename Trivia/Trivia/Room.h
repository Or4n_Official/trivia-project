#pragma once
#include <string>
#include <vector>
#include "LoginManager.h"

using std::string;
using std::vector;

struct RoomData
{
	unsigned int id;
	string name;
	unsigned int maxPlayers;
	unsigned int numOfQuestionsInGame;
	unsigned int timePerQuestion;
	unsigned int isActive;
};

class Room
{
public:

	Room();
	Room(RoomData roomdata, LoggedUser creatingUser);

	/*
	adds a user to the user vector
	input: loggeduser data
	output: none
	*/
	void addUser(LoggedUser user);

	/*
	removes a user from the user vector
	input: user to remove
	output: none
	*/
	void removeUser(LoggedUser user);

	/*
	this function returns a vector containing all of the users
	input: none
	output: a vector of all the logged users
	*/
	vector<string> getAllUsers();

	//getter
	RoomData getRoomData();

private:
	RoomData _metadata;
	vector<LoggedUser> _users;
};
