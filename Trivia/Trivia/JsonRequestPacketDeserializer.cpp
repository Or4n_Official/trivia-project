#include "JsonRequestPacketDeserializer.h"


string JsonRequestPacketDeserializer::bufferToString(unsigned char* buffer)
{
	string msg = "";
	for (int i = 0; i < SIZE - 1; i++)
	{
		msg += buffer[i];
	}
	return msg;
}

json JsonRequestPacketDeserializer::stringToJson(string buffer)
{

	string messageFromBuffer = "";
	for (int i = 5; i < SIZE - 5; i++)
	{
		messageFromBuffer += buffer[i];
	}
	json msgToReturn = json::parse(messageFromBuffer);
	return msgToReturn;
}

json JsonRequestPacketDeserializer::bufferToJson(unsigned char* buffer)
{
	string s = bufferToString(buffer);
	return stringToJson(s);
}

loginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(unsigned char* buffer)
{
	json dataJson = bufferToJson(buffer);
	struct loginRequest s;
	s.username = dataJson["username"];
	s.password = dataJson["password"];
	return s;
}

signupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(unsigned char* buffer)
{
	json dataJson = bufferToJson(buffer);
	struct signupRequest s;
	s.username = dataJson["username"];
	s.password = dataJson["password"];
	s.email = dataJson["email"];
	return s;
}

getPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayerRequest(unsigned char* buffer)
{
	json dataJson = bufferToJson(buffer);
	struct getPlayersInRoomRequest s;
	s.roomId = dataJson["roomId"];
	return s;
}

joinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(unsigned char* buffer)
{
	json dataJson = bufferToJson(buffer);
	struct joinRoomRequest s;
	s.roomId = dataJson["roomId"];
	return s;
}

createRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(unsigned char* buffer)
{
	json dataJson = bufferToJson(buffer);
	struct createRoomRequest s;
	s.maxUsers = dataJson["maxUsers"];
	s.answerTimeout = dataJson["answerTimeout"];
	s.questionCount = dataJson["questionCount"];
	s.roomName = dataJson["roomName"];
	return s;
}

submitAnswerRequest JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(unsigned char* buffer)
{
	json dataJson = bufferToJson(buffer);
	struct submitAnswerRequest s;
	s.answerId = dataJson["answerId"];
	return s;
}