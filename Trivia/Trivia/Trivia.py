import socket
import json

SERVER_PORT = 7777
SERVER_IP = "127.0.0.1"
LOGIN_REQUEST_CODE = 101
SIGNUP_REQUEST_CODE = 102


def main():
    client_msg_encoded = []
    msg_code = LOGIN_REQUEST_CODE

    msg_content_login = '{"username": "theBisMaker", "password": "drowssap"}'
    msg_content_signup = '{"username": "theBisMaker", "password": "drowssap", "email":"bis@gmail.com"}'

    msg_content_json = json.loads(msg_content_login)
    msg_content_json = json.dumps(msg_content_json)

    client_msg_encoded += int_to_bytes(msg_code, 1)
    client_msg_encoded += int_to_bytes(len(msg_content_json), 4)
    client_msg_encoded += msg_content_json.encode()

    # create tcp listening socket
    print(client_msg_encoded)
    msg2send = "".join([chr(value) for value in client_msg_encoded])
    print(msg2send)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (SERVER_IP, SERVER_PORT)
    sock.connect(server_address)
    sock.sendall(msg2send.encode())
    client_msg = sock.recv(1024)
    client_msg = client_msg.decode()
    print("message recieved is %s\n\n" % client_msg)
    sock.close()


def int_to_bytes(value, length):
    result = []

    for i in range(0, length):
        result.append(value >> (i * 8) & 0xff)

    result.reverse()

    return result


if __name__ == 'main':
    main()
