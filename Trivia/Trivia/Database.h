#pragma once
#include "sqlite3.h"
#include <string>

#define DB_FILE "triviaDB.sqlite"
#define UNDEFINED_ERROR 2
#define USER_EXISTS 1
#define USER_ADDED_SUCCESSFULLY 0
#define LOGIN_FAILED 2
#define USER_ALREADY_LOGGED_IN 1
#define LOGIN_SUCCESS 0

using std::string;

struct User
{
	int ID;
	string username;
	string password;
	string email;
};

class Database
{
public:
	Database();

	~Database();

	/*
	this method opens the database, in the case that the database doesn't exist yet, it creates it
	input: none
	output: 0 if everything is working well, else -1
	*/
	int open();

	/*
	this method checks if the username is present in the database
	input: a username
	output: true if the user exists, false if the user does not exist
	*/
	bool doesUserExist(string username);

	/*
	this function checks if the username and password of the user matches
	input: username and password
	output: true if a user with the same username and the same password match, else false
	*/
	bool doesPasswordMatch(string username, string password);

	/*
	this function creats a user
	input: username, password and email
	output: return code
	*/
	int createUser(string username, string password, string email);

private:

	/*
	this command sends the sql request and prints an error if there is a problemo
	input: sql statement
	output: 1 if an error accured, else 0
	*/
	int sendSqlRequest(string statement);

	/*
	an overload for requests that use callback functions
	input: sql statement, callback function pointer, and return value pointer
	output: 1 if an error accured, else 0
	*/
	int sendSqlRequest(string statement, int(*callback)(void*, int, char**, char**), void* data);

	sqlite3* _db;
	bool _isOpen;
};