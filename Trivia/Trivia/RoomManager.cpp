#include "RoomManager.h"

void RoomManager::createRoom(LoggedUser creatingUser, RoomData roomdata)
{
	std::pair<unsigned int, Room> newRoom(_currentID, Room(roomdata, creatingUser));
	this->_rooms.insert(newRoom);
	_currentID++;
}

void RoomManager::deleteRoom(unsigned int ID)
{
	_rooms.erase(ID);
}

unsigned int RoomManager::getRoomState(unsigned int ID)
{
	unsigned int state = _rooms[ID].getRoomData().isActive;
	return state;
}

vector<RoomData> RoomManager::getRooms()
{
	vector<RoomData> rooms;
	for (auto it = _rooms.begin(); it != _rooms.end(); it++)
	{
		rooms.push_back(it->second.getRoomData());
	}
	return rooms;
}
