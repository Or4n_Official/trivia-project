#include "Communicator.h"
#include "Database.h"
#include "RequestHandlerFactory.h"

class Server
{
public:
	Server();
	~Server();
	void run();

private:
	Communicator _communicator;
	RequestHandlerFactory* _factory;
	Database* _database;
};