#include "Database.h"
#include <io.h>
#include <iostream>
#include <list>
#include <exception>

#define OPEN_FAILED -1
#define BEGIN "BEGIN;"
#define ROLLBACK "ROLLBACK;"
#define COMMIT "COMMIT;"
#define SQL_REQ_FAILED 1

using std::string;
using std::list;

int callbackUsers(void* data, int argc, char** argv, char** azColName);

Database::Database()
	: _isOpen(false), _db(nullptr)
{
}

Database::~Database()
{
	if (_isOpen)
	{
		sqlite3_close(this->_db);
	}
}

int Database::open()
{
	string fileName = DB_FILE;
	int doesDatabaseExist = _access(fileName.c_str(), 0);
	int res = sqlite3_open(fileName.c_str(), &this->_db);
	if (res != SQLITE_OK)
	{
		this->_db = nullptr;
		std::cout << "failed to open database" << std::endl;
		return OPEN_FAILED;
	}
	if (doesDatabaseExist != 0) //in case the database is not initalized we will initialize it
	{
		string sqlStatement = "CREATE TABLE IF NOT EXISTS USERS ("
			"USERNAME TEXT PRIMARY KEY, "
			"PASSWORD TEXT, "
			"EMAIL TEXT);";
		int res = sendSqlRequest(sqlStatement);
		if (res == SQL_REQ_FAILED)
		{
			sqlite3_close(_db);
			remove(DB_FILE);
			return OPEN_FAILED;
		}
	}
	this->_isOpen = true;
	return 0;
}

bool Database::doesUserExist(string username)
{
	string statement;
	std::list<User> userList;
	statement = "SELECT * FROM USERS WHERE USERNAME = \"" + username + "\";";
	int (*callback) (void*, int, char**, char**);
	callback = callbackUsers;
	sendSqlRequest(statement, callback, &userList);
	return !(userList.empty());
}

bool Database::doesPasswordMatch(string username, string password)
{
	string statement;
	std::list<User> userList;
	statement = "SELECT * FROM USERS"
		" WHERE USERNAME = \"" + username + "\" AND PASSWORD = \"" + password + "\";";
	int (*callback) (void*, int, char**, char**);
	callback = callbackUsers;
	sendSqlRequest(statement, callback, &userList);
	return !(userList.empty());
}

int Database::createUser(string username, string password, string email)
{
	if (doesUserExist(username))
	{
		return USER_EXISTS;
	}
	string statement = "INSERT INTO USERS "
		"(USERNAME, PASSWORD, EMAIL) "
		"VALUES (\"" + username + "\", \"" + password + "\", \"" + email + "\");";
	int res = sendSqlRequest(statement);
	if (res == SQL_REQ_FAILED)
	{
		return UNDEFINED_ERROR;
	}
	return USER_ADDED_SUCCESSFULLY;
}

int Database::sendSqlRequest(string statement)
{
	int res = 0;
	char** errMessage = nullptr;
	res = sqlite3_exec(_db, BEGIN, nullptr, nullptr, errMessage);
	res = sqlite3_exec(_db, statement.c_str(), nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
	{
		printf("%s", sqlite3_errmsg(_db));
		sqlite3_exec(_db, ROLLBACK, nullptr, nullptr, errMessage);
		return 1;
	}
	sqlite3_exec(_db, COMMIT, nullptr, nullptr, errMessage);
	return 0;
}

int Database::sendSqlRequest(string statement, int(*callback)(void*, int, char**, char**), void* data)
{
	int res = 0;
	char** errMessage = nullptr;
	res = sqlite3_exec(_db, BEGIN, nullptr, nullptr, errMessage);
	res = sqlite3_exec(_db, statement.c_str(), callback, data, errMessage);
	if (res != SQLITE_OK)
	{
		printf("%s", sqlite3_errmsg(_db));
		sqlite3_exec(_db, ROLLBACK, nullptr, nullptr, errMessage);
		return 1;
	}
	sqlite3_exec(_db, COMMIT, nullptr, nullptr, errMessage);
	return 0;
}

int callbackUsers(void* data, int argc, char** argv, char** azColName)
{
	list<User>* userList = (list<User>*)data;
	User temp;
	for (int i = 0; i < argc; i++)
	{
		if (string(azColName[i]) == "ID")
		{
			temp.ID = std::stoi(argv[i]);
		}
		else if (string(azColName[i]) == "USERNAME")
		{
			temp.username = string(argv[i]);
		}
		else if (string(azColName[i]) == "PASSWORD")
		{
			temp.password = string(argv[i]);
		}
		else if (string(azColName[i]) == "EMAIL")
		{
			temp.email = string(argv[i]);
		}
		else
		{
			throw std::exception();
		}
	}
	userList->push_back(temp);
	return 0;
}
