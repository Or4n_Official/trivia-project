#pragma once
#include <map>
#include <vector>
#include "Room.h"

using std::map;

class RoomManager
{
public:
	/*
	this function adds a room to the rooms map
	input: the user who created and the room data
	output: none
	*/
	void createRoom(LoggedUser creatingUser, RoomData roomdata);
	
	/*
	removes a room from the room map
	input: room ID
	output: None
	*/
	void deleteRoom(unsigned int ID);

	/*
	returns the state of the room with the specified id
	input: room id
	output: room state
	*/
	unsigned int getRoomState(unsigned int ID);
	
	/*
	returns all the data of all the rooms
	input: none
	output: all of the rooms
	*/
	vector<RoomData> getRooms();

private:
	map<unsigned int, Room> _rooms;
	unsigned int _currentID = 0;
};