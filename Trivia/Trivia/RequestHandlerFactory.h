#pragma once
#include "LoginManager.h"
#include "Database.h"
#include "LoginRequestHandler.h"
class LoginRequestHandler;
class RequestHandlerFactory
{
private:
	LoginManager* _loginManager;
	Database* _database;
public:
	RequestHandlerFactory(Database* d);
	~RequestHandlerFactory();
	LoginRequestHandler* createLoginRequestHandler();
	LoginManager* getLoginManager();
};

