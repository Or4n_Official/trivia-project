#include "LoginManager.h"

LoginManager::LoginManager(Database* db)
	: _db(db)
{

}

int LoginManager::signup(string username, string password, string email)
{
	int retCode = _db->createUser(username, password, email);
	return retCode;
}

int LoginManager::login(string username, string password)
{
	bool success = _db->doesPasswordMatch(username, password);
	if (success)
	{
		for (auto it = _loggedUsers.begin(); it != _loggedUsers.end(); it++)
		{
			if (it->GetUsername() == username)
			{
				return USER_ALREADY_LOGGED_IN;
			}
		}
		_loggedUsers.push_back(LoggedUser(username));
		return LOGIN_SUCCESS;
	}
	return LOGIN_FAILED;
}

void LoginManager::logout(string username)
{
	for (std::vector<LoggedUser>::iterator it = this->_loggedUsers.begin(); it != this->_loggedUsers.end(); it++)
	{
		if (it->GetUsername() == username)
		{
			_loggedUsers.erase(it);
			break;
		}
	}
}

LoggedUser::LoggedUser(string username)
	: _username(username)
{
}

string LoggedUser::GetUsername() const
{
	return this->_username;
}

bool LoggedUser::operator==(const LoggedUser& rhs)
{
	return this->GetUsername() == rhs.GetUsername();
}
