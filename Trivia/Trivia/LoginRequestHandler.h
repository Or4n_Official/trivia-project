#pragma once
#include "RequestHandlerFactory.h"
#include "IRequestHandler.h"
#include "LoginRequestHandler.h"
#include "JsonRequestPacketDeserializer.h"


#define SIGNUP_ID 1
#define LOGIN_ID 2
#define LOGIN_SUCCESSFUL 0


class RequestHandlerFactory;
class LoginRequestHandler : public IRequestHandler
{
private:
	LoginManager* _loginManager;
	RequestHandlerFactory* _factory;
	RequestResult login(RequestInfo info);
	RequestResult signup(RequestInfo info);

	
public:
	LoginRequestHandler(LoginManager* LoginManager, RequestHandlerFactory* handlerFactory);

	virtual bool isRequestRelevant(RequestInfo info) override;
	virtual RequestResult HandleRequest(RequestInfo info) override;
};

