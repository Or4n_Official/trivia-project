#include "Server.h"
#include <iostream>
#include <thread>

#define MAXLEN 100



Server::Server()
{
	_database = new Database;
	_database->open();
	_factory = new RequestHandlerFactory(_database);
	_communicator = Communicator(_factory);
}

Server::~Server()
{
	delete _factory;
	delete _database;
}

void Server::run()
{
	std::thread communicatorThread(&Communicator::startHandleRequests, &_communicator);
	communicatorThread.detach();
	char consoleInput[MAXLEN] = { 0 };
	while (std::string(consoleInput) != "EXIT")
	{
		std::cin.getline(consoleInput, MAXLEN - 1);
	}
}
