#include "PacketSerializer.h"
#include <iostream>

#define LOGIN_RESPONSE 111
#define SIGNUP_RESPONSE 112
#define ERROR_RESPONSE 211

BYTE_STREAM intToBytes(int paramInt) //COPIED STRAIGNT FROM STACK OVERFLOW
{
	BYTE_STREAM arrayOfByte(4);
	for (int i = 0; i < 4; i++)
		arrayOfByte[3 - i] = (paramInt >> (i * 8));
	return arrayOfByte;
}

BYTE_STREAM JsonResponsePacketSerializer::serializeResponse(responses::LoginResponse response)
{
	json j;
	j["status"] = response.status;
	return makeByteStream(j, LOGIN_RESPONSE);
}

BYTE_STREAM JsonResponsePacketSerializer::serializeResponse(responses::SignupResponse response)
{
	json j;
	j["status"] = response.status;
	return makeByteStream(j, SIGNUP_RESPONSE);
}

BYTE_STREAM JsonResponsePacketSerializer::serializeResponse(responses::ErrorResponse response)
{
	json j;
	j["message"] = response.message;
	return makeByteStream(j, ERROR_RESPONSE);
}

BYTE_STREAM JsonResponsePacketSerializer::makeByteStream(json j, int msgCode)
{
	string jsonString = j.dump();
	BYTE_STREAM jsonBytes(jsonString.begin(), jsonString.end());
	BYTE_STREAM size = intToBytes(jsonString.length());
	BYTE_STREAM code(1);
	code[0] = LOGIN_RESPONSE;
	BYTE_STREAM bytes; //starting to connect the byte streams together
	bytes.reserve(code.size() + size.size() + jsonBytes.size());
	bytes.insert(bytes.end(), code.begin(), code.end());
	bytes.insert(bytes.end(), size.begin(), size.end());
	bytes.insert(bytes.end(), jsonBytes.begin(), jsonBytes.end());
	bytes.push_back('\0');
	return bytes;
}
