#include "Communicator.h"
#include "PacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include <iostream>
#include <vector>
#include <string.h>

#define SIZE 1024
#define LOGINREQ 101
#define SIGNUPREQ 102
#define NOMSG 0

using std::string;

int sendMsg(SOCKET clientSock, string msg)
{
	return send(clientSock, msg.c_str(), msg.length(), 0);
}

BYTE_STREAM recvMsg(SOCKET clientSock)
{
	char cmsg[SIZE] = { 0 };
	recv(clientSock, cmsg, SIZE, 0);
	BYTE_STREAM bytes;
	for (int i = 0; i < SIZE; i++)
	{
		bytes.push_back(cmsg[i]);
	}
	return bytes;
}



Communicator::Communicator()
{
}

Communicator::Communicator(RequestHandlerFactory* f)
	: _factory(f)
{
}

Communicator::~Communicator()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Communicator::startHandleRequests()
{
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
	bindAndListen();
}

void Communicator::bindAndListen()
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(PORT); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
	{
		std::cout << WSAGetLastError() << std::endl;
		throw std::exception(__FUNCTION__ " - bind");
	}

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << PORT << std::endl;
	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		// this accepts the client and create a specific socket from server to this client
		SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

		if (client_socket == INVALID_SOCKET)
			throw std::exception(__FUNCTION__);

		std::cout << "Client accepted. Server and client can speak" << std::endl;
		IRequestHandler* requestHandler;
		std::pair<SOCKET, IRequestHandler*> client(client_socket, requestHandler);
		this->_clients.insert(client);
		// the function that handle the conversation with the client
		std::thread newthread(&Communicator::handleNewClient, this, client_socket);
		newthread.detach();
	}
}

void Communicator::handleNewClient(SOCKET clientSock)
{
	int oran = 0, id = 0;
	BYTE_STREAM input, response;
	LoginRequestHandler* logHandler = _factory->createLoginRequestHandler();
	try
	{
		while (oran == 0)
		{
			input = recvMsg(clientSock);
			switch (input[0])
			{
			case NOMSG:
				break;
			case LOGINREQ:
				id = LOGIN_ID;
				break;
			case SIGNUPREQ:
				id = SIGNUP_ID;
				break;
			default:
				std::cout << "not supported YET" << std::endl;
			}
			if (input[0] == 0)
			{
				break;
			}
			time_t t = time(NULL);
			RequestInfo info = { id, t, input };
			RequestResult result = logHandler->HandleRequest(info);
			string s2s(result.buffer.begin(), result.buffer.end());
			sendMsg(clientSock, s2s);
		}
	}
	catch (std::exception&)
	{
		std::cout << "ERROR ACCURRED WITH CLIENT HANDLER OR SOMETHING" << std::endl;
	}
	delete logHandler;
}
