#pragma once
#include <vector>
#include <string>
#include <nlohmann/json.hpp>

#define BYTE_STREAM std::vector<unsigned char>

using std::string;
using json = nlohmann::json;

namespace responses
{
	struct LoginResponse
	{
		unsigned int status;
	};

	struct SignupResponse
	{
		unsigned int status;
	};

	struct ErrorResponse
	{
		string message;
	};
}

class JsonResponsePacketSerializer
{
public:
	
	/*
	this method converts the login response to a byte stream
	input: login response
	output: response byte stream
	*/
	static BYTE_STREAM serializeResponse(responses::LoginResponse response);
	
	/*
	this method converts the signup response to a byte stream
	input: signup response
	output: response byte stream
	*/
	static BYTE_STREAM serializeResponse(responses::SignupResponse response);

	/*
	this method converts the error response to a byte stream
	input: error response
	output: response byte stream
	*/
	static BYTE_STREAM serializeResponse(responses::ErrorResponse response);

private:
	static BYTE_STREAM makeByteStream(json j, int msgCode);
};