#include "RequestHandlerFactory.h"



RequestHandlerFactory::RequestHandlerFactory(Database* d)
{
    _database = d;
    _loginManager = new LoginManager(_database);
}

RequestHandlerFactory::~RequestHandlerFactory()
{
    try
    {
        delete _loginManager;
    }
    catch(std::exception&)
    { }
}

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
    LoginRequestHandler* r = new LoginRequestHandler(this->_loginManager, this);
    return r;
}

LoginManager* RequestHandlerFactory::getLoginManager()
{
    return this->_loginManager;
}